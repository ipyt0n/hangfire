﻿using System;
using System.Linq;
using Hangfire;

namespace hangfire_web.infra
{
    public class HangfireRecurringJobScheduler
    {

        private const string _randomEvery2Min = "RandomEvery2Min";
        private const string _randomEvery5Min = "RandomEvery5Min";
        private const string _randomEveryDaily = "RandomEveryDaily";
        private static Random random = new Random();
        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static void ScheduleJobs()
        {
            string text1 = string.Format("{0},  {1} ", RandomString(6), DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
            string text2 = string.Format("{0},  {1} ", RandomString(10), DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
            RecurringJob.RemoveIfExists(_randomEvery2Min);
            RecurringJob.AddOrUpdate<IScheduleJobs>(
                _randomEvery2Min,
                job => job.SendMessageOnRandomAsync_1(text1),
                "*/2 * * * *");

            RecurringJob.AddOrUpdate<IScheduleJobs>(
               _randomEvery5Min,
               job => job.SendMessageOnRandomAsync_2(text2),
               "*/5 * * * *");

            RecurringJob.AddOrUpdate<IScheduleJobs>(
               _randomEveryDaily,
               job => job.SendMessageOnRandomAsync_continue(text2),Cron.Daily);
        }


    }
}
