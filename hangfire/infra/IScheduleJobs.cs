﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace hangfire_web.infra
{
    public interface IScheduleJobs
    {
        void SendMessageOnRandomAsync_1(string message);
        void SendMessageOnRandomAsync_2(string message);
        void SendMessageOnRandomAsync_continue(string message);
    }

    public class ScheduleJobs : IScheduleJobs
    {

        public void SendMessageOnRandomAsync_continue(string message)
        {
            for (var i = 1; i <= 120; i++)
            {
                Console.WriteLine(i + message);
                Thread.Sleep(1000);
            }
           
        }


        public void SendMessageOnRandomAsync_1(string message)
        {
            Console.WriteLine(message);
        }


        public void SendMessageOnRandomAsync_2(string message)
        {
            Console.WriteLine(message);
        }

    }
}
