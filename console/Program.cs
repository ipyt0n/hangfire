﻿using System;
using System.IO;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace console
{
    class Program
    {
        private static IConfiguration _configuration { get; set; }

        public static async Task Main(string[] args)
        {
            ConfigureApplication();

            //RecurringJob.AddOrUpdate("console Recurring",
            //    () => Console.WriteLine("Recurring!"),
            //    Cron.Daily);

            //RecurringJob.AddOrUpdate<IScheduleJobs>(
            //"console Recurring",
            //job => job.SendMessageOnRandomAsync_console("Hello Test"),
            //"*/2 * * * *");

            await Console.Out.WriteLineAsync("HangFire Processing Server has started. Press any key to exit...");
            await Console.In.ReadLineAsync();

        }

        private static void ConfigureApplication()
        {
            var conn = "Server=128.199.74.163;Database=hangfire_test;User ID=sa;Password=BudDy@Passw0rd;Trusted_Connection=false;";

            GlobalConfiguration
                .Configuration
                .UseColouredConsoleLogProvider()
                .UseSqlServerStorage(conn);
        }

    }
}
